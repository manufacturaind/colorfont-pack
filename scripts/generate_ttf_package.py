#!/usr/bin/env python
# 
# Colorfont TTF generator
# =======================
# 
# Copyright 2012 Manufactura Independente (Ana Carvalho & Ricardo Lafuente)
# 

import sys, os, shutil
import fnmatch
import fontforge
from utils import createZip
from pprint import pprint

def font2ttf(in_filename, out_filename):
    font = fontforge.open(in_filename)
    font.generate(out_filename)

scriptpath = os.path.dirname(os.path.abspath(__file__)) 
basepath = os.path.abspath(os.path.join(scriptpath, '..'))
fontspath = os.path.join(scriptpath, '..', 'fonts/')
fontdirs = os.listdir(fontspath)

files_to_convert = []
files_to_copy = []

for dirname in fontdirs:
    d = os.path.join(fontspath, dirname)
    for file in os.listdir(d):
        if fnmatch.fnmatch(file, '*.sfd'):
            files_to_convert.append(os.path.join(dirname, file))
        elif fnmatch.fnmatch(file, '*.ttf') or fnmatch.fnmatch(file, '*.otf'):
            files_to_copy.append(os.path.join(dirname, file))

OUTDIR = os.path.abspath(os.path.join(scriptpath, '..', 'colorfonts'))
if os.path.exists(OUTDIR):
    shutil.rmtree(OUTDIR)

os.mkdir(OUTDIR)

for d in fontdirs:
    os.mkdir(os.path.join(OUTDIR, d))

for file in files_to_convert:
    infile = os.path.join(fontspath, file)
    outfile = os.path.join(OUTDIR, file.replace('sfd', 'ttf'))
    font2ttf(infile, outfile)

for file in files_to_copy:
    infile = os.path.join(fontspath, file)
    outfile = os.path.join(OUTDIR, file)
    shutil.copyfile(infile, outfile)

shutil.copyfile(os.path.join(scriptpath, '..', 'README'), os.path.join(OUTDIR, 'README'))

createZip(OUTDIR, os.path.join(basepath, 'colorfonts.zip'))

shutil.rmtree(OUTDIR)
