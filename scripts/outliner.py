import fontforge

font = fontforge.open('Bazar.ttf')

for glyph in font:
    # expand stroke
    font[glyph].stroke('circular',  # pen type
                       50,          # width
                       'square',    # line cap
                       'bevel',     # line join
                       ('cleanup',) # extra flags
                      )

font.fontname += 'Outline'
font.generate('bazar-gorda.ttf')
